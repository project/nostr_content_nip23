<?php

namespace Drupal\nostr_content_nip23\Services;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for Nostr publish event.
 */
interface NostrPublishInterface {

  /**
   * Send a message to the Nostr Network.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity used for the message.
   * @param int $uid
   *   The user id.
   * @param string $public_key
   *   The public key.
   * @param string $private_key
   *   The private key.
   * @param array $relays
   *   The relay.
   * @param bool $show_drupal_message
   *   Whether to add drupal messages. Set to FALSE in case this service is
   *   called in a non ui context.
   */
  public function sendMessage(EntityInterface $entity, int $uid, string $public_key, string $private_key, array $relays, bool $show_drupal_message);

}
