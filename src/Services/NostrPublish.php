<?php

namespace Drupal\nostr_content_nip23\Services;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use swentel\nostr\Event\Event;
use swentel\nostr\Message\EventMessage;
use swentel\nostr\Relay\CommandResult;
use swentel\nostr\Relay\Relay;
use swentel\nostr\Relay\RelaySet;
use swentel\nostr\Sign\Sign;

/**
 * Nostr publish event class.
 */
class NostrPublish implements NostrPublishInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $currentUser;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * Nostr storage service.
   *
   * @var \Drupal\nostr_content_nip23\Services\NostrStorageInterface
   */
  protected NostrStorageInterface $nostrStorage;

  /**
   * Logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * Show Drupal message or not.
   *
   * @var bool
   */
  protected bool $showDrupalMessage = FALSE;

  /**
   * NostrPublish constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger.
   * @param \Drupal\nostr_content_nip23\Services\NostrStorageInterface $nostr_storage
   *   The nostr storage.
   */
  public function __construct(AccountInterface $current_user, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger, NostrStorageInterface $nostr_storage) {
    $this->currentUser = $current_user;
    $this->nostrStorage = $nostr_storage;
    $this->messenger = $messenger;
    $this->loggerChannelFactory = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function sendMessage(EntityInterface $entity, int $uid, string $public_key, string $private_key, array $relays, bool $show_drupal_message) {
    $this->showDrupalMessage = $show_drupal_message;

    $summary = '';
    // Get content from a field.
    $content = NULL;
    $content_fields = Settings::get('nostr_longform_content_field', ['body']);
    foreach ($content_fields as $content_field) {
      if ($entity->hasField($content_field)) {
        $summary = trim($entity->get($content_field)->summary);
        $content = trim($entity->get($content_field)->value);
        break;
      }
    }
    // Replace inline relative URLs to media / images with absolute URLs in this content field.
    $content = preg_replace('/^\/sites\/default\/files\//mi', \Drupal::request()->getSchemeAndHttpHost().'/$0', $content);

    if (!isset($content)) {
      $this->showDrupalMessage($this->t('No content found to send to the Nostr network.'), 'warning');
      $this->loggerChannelFactory->get('nostr_content_nip23.publish')->warning($this->t('No content found in the fields to send to the Nostr network for @entity_type_id @entity_id', [
        '@entity_type_id' => $entity->getEntityTypeId(),
        '@entity_id' => $entity->id(),
      ]));
      return;
    }

    // Get optional summary text from field_summary.
    if ($summary === '' && $entity->hasField('field_summary')) {
      $summary = trim($entity->get('field_summary')->value);
    }

    // Get optional image from field with machine name field_image from the entity.
    if ($entity->hasField('field_image') && !empty($entity->get('field_image')->referencedEntities())) {
      $image_field = $entity->get('field_image')->referencedEntities()[0];
      if ($image_field !== NULL && $image_field->getEntityTypeId() === 'media') {
        /** @var int $file_id */
        $file_id = $image_field->getSource()->getSourceFieldValue($image_field);
        /** @var \Drupal\file\Entity\File $file */
        $file = File::load($file_id);
        if ($file) {
          /** @var string $image_absolute_url */
          $image_absolute_url = $file->createFileUrl(FALSE);
        }
      }
    }

    /**
     * Example Event
     * See https://github.com/nostr-protocol/nips/blob/master/23.md#example-event
     */
    $event = new Event();
    // If this content is not published, change the kind to 30024 (long form draft)
    $eventKind = ($entity->isPublished()) ? 30023 : 30024;
    $event->setKind($eventKind);
    $event->setPublicKey($public_key);
    $event->setTags([
      [ 'title', $entity->get('title')->value ],
      [ 'published_at', sprintf('%s', $entity->get('created')->value) ],
      [ 'L', '#t' ],
      [ 'l', '#Drupal', '#t' ],
      [ 'client', (\Drupal::request()->getHost() !== 'localhost') ? \Drupal::request()->getSchemeAndHttpHost() : 'Drupal'],
    ]);
    // Add image
    if (isset($image_absolute_url)) {
      $event->addTag([ 'image', $image_absolute_url ]);
    }
    // Add summary
    if (isset($summary)) {
      $event->addTag([ 'summary', $summary ]);
    }
    if ($event_id = $this->nostrStorage->getEventId($entity->id(), $entity->getEntityTypeId())) {
      $event->setId($event_id);
    }
    $event->setContent($content);

    // Get alias slug of saved entity which we will use as a unique identifier for the replaceable Nostr event (in the d-tag).
    $alias_cleaner = \Drupal::service('pathauto.alias_cleaner');
    $dTag = $alias_cleaner->cleanString($entity->get('title')->value);
    // TODO: we need to keep the d-tag the same when we update the event, so using a slug is not a ideal solution here.
    $event->addTag([ 'd', sprintf('%s', $dTag) ]);
    // The a-tag is a reference to another event
    //$event->addTag([ 'a', $eventKind . ':' . $public_key . ':' . $dTag, $relays['outbox'][0]]);
    // Add canonical URL as a tag to this event. This tag is not part of the Nostr specs, do most relays won't accept this.
    // $event->addTag([ 'canonical', \Drupal::request()->getSchemeAndHttpHost() . '/' . $dTag ]);

    /**
     * Publish to these predefined relays.
     * These relays which support NIP-23 and are used among the most popular long-form content Nostr clients.
     * Source: https://njump.me/naddr1qqyrjcnpxsmrgcesqy28wumn8ghj7un9d3shjtnyv9kh2uewd9hsygrxl93dnvvmdlsftgadpcc9xsjgnmx958p6r0qpxjlanz9w2snvwqpsgqqqw4rsq27597
     */
    // TODO check for duplicate relays urls in $nip23_supported_relays and $relays['outbox]
    $nip23_supported_relays = [
      'wss://relay.damus.io/',
      'wss://nos.lol',
      'wss://relay.nostr.band',
//      'wss://relay.highlighter.com/',
//      'wss://nostr-01.yakihonne.com',
    ];
    $relaySet = new RelaySet();
    if (!empty($nip23_supported_relays)) {
      foreach ($nip23_supported_relays as $pre_def_relayUrl) {
        $relay = new Relay($pre_def_relayUrl);
        $relaySet->addRelay($relay);
        $event->addTag(['r', $pre_def_relayUrl]);
      }
    }

    // Add user defined relays to the RelaySet.
    foreach ($relays['outbox'] as $relayUrl) {
      $relay = new Relay($relayUrl);
      $relaySet->addRelay($relay);
      $event->addTag(['r', $relayUrl]);
    }
    // Sign the event.
    $signer = new Sign();
    $signer->signEvent($event, $private_key);
    $eventMessage = new EventMessage($event);
    $relaySet->setMessage($eventMessage);

    try {
      /** @var [] $response */
      $relayResponses = $relaySet->send();
      foreach ($relayResponses as $response) {
        if ($this->currentUser->hasPermission('view nostr debugging')) {
          $this->showDrupalMessage(print_r($response, 1), 'warning');
        }
        if ($response->isSuccess() === FALSE) {
          $failure = $response->message();
          $this->showDrupalMessage($this->t('Failed posting event to Nostr network: @fail', ['@fail' => $failure]), 'error');
          $this->loggerChannelFactory->get('nostr_content_nip23.publish')->error($this->t('Failed posting to Nostr network: @message', ['@message' => $failure]));
        } else {
          // Store the event id.
          if (Settings::get('nostr_nip23_sent_nids', TRUE)) {
            $this->nostrStorage->storeEventId($entity->id(), $entity->getEntityTypeId(), $event->getId(), $uid);
          }
        }
      }
    }
    catch (\Exception $e) {
      $this->showDrupalMessage($this->t('Exception error: @message', ['@message' => $e->getMessage()]), 'error');
      $this->loggerChannelFactory->get('nostr_content_nip23.publish')->error($this->t('Exception error: @message', ['@message' => $e->getMessage()]));
    }
  }

  /**
   * Helper function to show a Drupal message.
   *
   * @param string $message
   *   The message.
   * @param string $type
   *   Type of message.
   */
  protected function showDrupalMessage(string $message, string $type = 'default') {
    if ($this->showDrupalMessage) {
      switch ($type) {
        case 'error':
          $this->messenger->addError($message);
          break;

        case 'warning':
          $this->messenger->addWarning($message);
          break;

        default:
          $this->messenger->addMessage($message);
          break;
      }
    }
  }
}
