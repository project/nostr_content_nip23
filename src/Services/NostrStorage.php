<?php

namespace Drupal\nostr_content_nip23\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;

/**
 * Nostr storage class.
 */
class NostrStorage implements NostrStorageInterface {

  use DependencySerializationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Constructs a NostrStorage object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function storeEventId(int $entity_id, string $entity_type_id, string $event_id, int $uid) {
    $values = [
      'entity_id' => $entity_id,
      'entity_type_id' => $entity_type_id,
      'event_id' => $event_id,
      'uid' => $uid,
    ];
    try {
      $this->connection->insert('nostr_content_nip23')->fields(array_keys($values), array_values($values))->execute();
    }
    catch (\Exception) {
      // No need to log or show a message. The site has bigger problems if the
      // database is down.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEventId(int $entity_id, string $entity_type_id): string|FALSE {
    return $this->connection->select('nostr_content_nip23', 'ncn')
      ->fields('ncn', ['event_id'])
      ->condition('entity_id', $entity_id)
      ->condition('entity_type_id', $entity_type_id)
      ->execute()
      ->fetchField();
  }

}
