<?php

namespace Drupal\nostr_content_nip23\Services;

use Drupal\Core\Form\FormStateInterface;

/**
 * Nostr form alter interface.
 */
interface NostrFormAlterInterface {

  /**
   * Adds the Nostr form element on the entity form.
   *
   * @param array $form
   *   Form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form state.
   */
  public function addNostrFormElement(array &$form, FormStateInterface $form_state);

}
