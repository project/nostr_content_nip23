# Nostr long-form content [NIP-23](https://github.com/nostr-protocol/nips/blob/master/23.md)

https://github.com/nostr-protocol/nips/blob/master/23.md

Currently only works for a single Drupal user.

## Requirements

- PHP 8.1+ and the GMP extension - https://www.php.net/manual/en/ref.gmp.php

## How it works

Adds a 'Publish to Nostr' fieldset to node forms, protected by the
'publish to nostr network' permission. It currently assumes the content of
your post is stored in the 'body' field, but this can be overridden by a
setting.

Once the node has been sent, you can't publish it anymore. The event id is
stored in the nostr_content_nip23 table.

There's an extra permission to view debug information after trying to post to
the network.

## Configuration

Posts to the network need to be signed, so we need the public and private key
of your Nostr account and one relay.

```
// Path to your Nostr private key file.
// Put these files somewhere outside your webroot!
// The values of the keys are the hex encoded versions, not bech32 (which
// either start with npub or nsec).
// Use https://damus.io/key/ or https://github.com/rot13maxi/key-convertr
// to convert.
$settings['nostr_private_key_file'] = '/path/to/private-key-file';
```

```
// Nostr relays - find other relays at https://nostr.watch/relays/find.
// This relay seems to work fine. In case you are not connected with this one,
// add it to your relays so posts show up in your client as well.
$settings['nostr_relays'] = [ 'outbox' => [ 'wss://relay.damus.io'] ];
```

```
// Content fields - defaults to 'body'.
$settings['nostr_longform_content_field'] = ['field_markdown'];

// Other optional content type fields:
* field_image - should be a Media field using a image
* field_summary - short summary in plain text format
```

## Roadmap

* [x] Add image together with a Drupal image field from the entity
* [x] Add summary field
* [x] Support multiple relays instead of one
* [x] When published, add option to republish (kind 30023 is replaceable with the `d` identifiable tag)
* [x] Add draft support to publish the event with kind 30024
* [ ] Add a tag with origin value (from which URL the article is published)
* [x] Add tag label #Drupal to each published event for discovering content published by a Drupal powered system
* [ ] Add a markdown editor dependency which saves the output as MarkDown
* [x] Replace relative / inline media URL's with absolute URL's in the Markdown content
* [ ] Get or generate event identifier (`nevent` bech32-encoded entity) after it's published (NIP-19)
* [ ] Add delete option
* [ ] Add predefined configurations which will be installed when you install / update the module
  * [ ] `field_nsec` which can be added to the user entity (integrated with the contrib module [field_encrypt](https://git.drupalcode.org/project/field_encrypt))
  * [ ] `field_relays` which can be added to the user entity
  * [ ] Markdown CKEditor5 text editor for the content field
  * [ ] Markdown formatter / renderer for displaying the content field on the website

## Related contrib modules

* [Nostr simple publish](https://www.drupal.org/project/nostr_simple_publish)
* [Nostr internet identifier NIP-05](https://www.drupal.org/project/nostr_id_nip05)
